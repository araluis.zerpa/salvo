package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class SalvoController {

    //Autowired para cada repositorio a utilizar
    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    /*Cada mapping realizado se muestra a continuación y posteriormente sus maps
    y lists correspondientes según sea el caso*/

    //Se realiza un nuevo Mapping para el Authentication para jugadores (Login)
    @RequestMapping("/games")
    public Map<String, Object> makeLoggedPlayer(Authentication authentication){
        Map<String, Object> dto = new LinkedHashMap<>();
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
            dto.put("player","Guest");
        else
            dto.put("player", loggedPlayerDTO(playerRepository.findByUserName(authentication.getName())));
        dto.put("games", getGames());
        return dto;
    }

    public Map<String, Object> loggedPlayerDTO(Player player){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id",player.getId());
        dto.put("name", player.getUserName());
        return dto;
    }


    public List<Map<String, Object>> getGames() {
        { return gameRepository
                .findAll()
                .stream()
                .map(game -> makeGameDTO(game))
                .collect(Collectors.toList());
        }
    }

    private Map<String, Object> makeGameDTO(Game game) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", game.getId());
        dto.put("creationDate", game.getCreationDate());
        dto.put("gamePlayers", getGamePlayerList(game.getGamePlayers()));
        dto.put("scores", getScores(game.getGamePlayers()));
        return dto;
    }

    private List<Map<String, Object>> getScores(List<GamePlayer> gamePlayers) {
        { return gamePlayers
                .stream()
                .map(gamePlayer -> makeScoreDTO(gamePlayer))
                .collect(Collectors.toList());
        }
    }

    private Map<String, Object> makeScoreDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerID", gamePlayer.getPlayer().getId());
        if(gamePlayer.getScore() != null)
            dto.put("score", gamePlayer.getScore().getScore());
        return dto;
    }

    private List<Map<String, Object>> getGamePlayerList(List<GamePlayer> gamePlayers) {
        { return gamePlayers
                .stream()
                .map(gamePlayer -> makeGamePlayerDTO(gamePlayer))
                .collect(Collectors.toList());
        }
    }

    private Map<String, Object> makeGamePlayerDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getId());
        dto.put("joinDate", gamePlayer.getJoinDate());
        dto.put("player", makePlayerDTO(gamePlayer.getPlayer()));
        return dto;
    }

    private Map<String, Object> makePlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("userName", player.getUserName());
        dto.put("score", getScoreList(player));
        return dto;
    }


    //Mapping para crear jugadores verificando si hay jugadores con emails repetidos
    // y cuando algún campo está vacío. Si funciona, se crea un nuevo player.
    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Object> createPlayer(@RequestParam String username, @RequestParam String password) {

        if (username.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>("Missing data :(", HttpStatus.FORBIDDEN);

        }
        if (playerRepository.findOneByUserName(username) !=  null) {
            return new ResponseEntity<>("Name already in use, sorry :(", HttpStatus.FORBIDDEN);
        }

        Player newPlayer = new Player(username, passwordEncoder.encode(password));
        playerRepository.save(newPlayer);
        return new ResponseEntity<>(makeMap("id", newPlayer.getId()),HttpStatus.CREATED);
    }

    //MakeMap para los ResponseEntity
    public Map<String, Object> makeMap(String key, Object value){
        Map<String,Object> map = new LinkedHashMap<>();
        map.put(key,value);
        return map;
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    //Para crear nuevos juegos verificando que está loggeado el player.
    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createGame(Authentication authentication) {
        if (isGuest(authentication)) {
            return new ResponseEntity<>( makeMap("error","Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

        //Crear y guardar nuevos juegos
        Game newGame = gameRepository.save(new Game(new Date()));
        //Crear y guardar nuevos GamePlayer
        GamePlayer newGamePlayer = gamePlayerRepository.save(new GamePlayer(newGame, playerRepository.findByUserName(authentication.getName())));

        return new ResponseEntity<>( makeMap("gpid", newGamePlayer.getId()), HttpStatus.CREATED);
    }


    //Para unirse a juegos en espera
    @RequestMapping(path = "/game/{id}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> joinGame(Authentication authentication, @PathVariable long id) {
        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        Optional<Game> game = gameRepository.findById(id);
        if (!game.isPresent())
            return new ResponseEntity<>(makeMap("error", "This Game Doesn't Exist"), HttpStatus.FORBIDDEN);

        if(game.get().getGamePlayers().size() > 1){
            return new ResponseEntity<>(makeMap("error", "This Game Is Full"), HttpStatus.FORBIDDEN);
        }
        Player newPlayer = playerRepository.findByUserName(authentication.getName());
        GamePlayer newGamePlayer = gamePlayerRepository.save(new GamePlayer(game.get(),newPlayer));

        return new ResponseEntity<>(makeMap("gpid", newGamePlayer.getId()), HttpStatus.CREATED);
    }



    /*Mapping del game_view con sus respectivas autorizacionespara poder bloquear
    la vista de otros juegos para los usuarios que no son participantes */
    @RequestMapping("/game_view/{gpid}")
    public ResponseEntity<Object> cheat(@PathVariable long gpid, Authentication authentication) {
        Player newPlayer = getLoggedPlayer(authentication);
        GamePlayer gamePlayer = gamePlayerRepository.findById(gpid).orElse(null);

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "Forbidden"), HttpStatus.FORBIDDEN);
        }

        if (newPlayer.getId() != gamePlayer.getPlayer().getId()) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

        //Método para obtener al GamePlayer oponente del GamePlayer loggeado
        Optional<GamePlayer> opponentGamePlayer = gamePlayer.getGame().getGamePlayers().stream().filter(gpo -> gpo.getId() != gpid).findFirst();
        GamePlayer getOpponentGamePlayer = opponentGamePlayer.get();

        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("creationDate", gamePlayer.getGame().getCreationDate().getTime());
        dto.put("gamePlayers", getGamePlayerList(gamePlayer.getGame().getGamePlayers()));
        dto.put("ships", getShipList(gamePlayer.getShips()));
        dto.put("salvoes", getSalvoList(gamePlayer.getGame()));
        dto.put("hits", makeHitsDTO(gamePlayer, getOpponentGamePlayer));
        return new ResponseEntity<>(dto, HttpStatus.OK);

    }

    Player getLoggedPlayer(Authentication authentication){
        return playerRepository.findByUserName(authentication.getName());
    }

    //Para los hits de cada player en juego:
    private Map<String, Object> makeHitsDTO(GamePlayer selfGP, GamePlayer opponentGP){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(selfGP, opponentGP));
        dto.put("opponent", getHits(opponentGP, selfGP));
        return dto;
    }

    private List<Map> getHits(GamePlayer gamePlayer, GamePlayer opponentGameplayer) {
        List<Map> hits = new ArrayList<>();
        Integer carrierDamage = 0;
        Integer battleshipDamage = 0;
        Integer submarineDamage = 0;
        Integer destroyerDamage = 0;
        Integer patrolboatDamage = 0;
        List <String> carrierLocation = new ArrayList<>();
        List <String> battleshipLocation = new ArrayList<>();
        List <String> submarineLocation = new ArrayList<>();
        List <String> destroyerLocation = new ArrayList<>();
        List <String> patrolboatLocation = new ArrayList<>();
        gamePlayer.getShips().forEach(ship -> {
            switch (ship.getShipType()) {
                case "carrier":
                    carrierLocation.addAll(ship.getShipLocations());
                    break;
                case "battleship":
                    battleshipLocation.addAll(ship.getShipLocations());
                    break;
                case "submarine":
                    submarineLocation.addAll(ship.getShipLocations());
                    break;
                case "destroyer":
                    destroyerLocation.addAll(ship.getShipLocations());
                    break;
                case "patrolboat":
                    patrolboatLocation.addAll(ship.getShipLocations());
                    break;
            }
        });
        for (Salvo salvo : opponentGameplayer.getSalvoes()) {
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0;
            Integer missedShots = salvo.getSalvoLocations().size();
            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();
            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();
            salvoLocationsList.addAll(salvo.getSalvoLocations());
            for (String salvoShot : salvoLocationsList) {
                if (carrierLocation.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (battleshipLocation.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (submarineLocation.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (destroyerLocation.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (patrolboatLocation.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
            }
            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("carrier", carrierDamage);
            damagesPerTurn.put("battleship", battleshipDamage);
            damagesPerTurn.put("submarine", submarineDamage);
            damagesPerTurn.put("destroyer", destroyerDamage);
            damagesPerTurn.put("patrolboat", patrolboatDamage);
            hitsMapPerTurn.put("turn", salvo.getTurn());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            hits.add(hitsMapPerTurn);
        }
        return hits;
    }

    private Map<String,Object> makeshipDTO(Ship ship){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("shipType", ship.getShipType());
        dto.put("shipLocations", ship.getShipLocations());
        return dto;
    }

    private List<Map<String, Object>> getShipList(List<Ship> ships) {
        { return ships
                .stream()
                .map(ship -> makeshipDTO(ship))
                .collect(Collectors.toList());
        }
    }

    private Map<String,Object> salvoDTO(Salvo salvo){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        dto.put("salvoLocations", salvo.getSalvoLocations());
        return dto;
    }

    private List<Map<String, Object>> makeSalvoList(List<Salvo> salvoes) {
        return salvoes
                .stream()
                .map(salvo -> salvoDTO(salvo))
                .collect(Collectors.toList());
    }

    /*Lista para que se linkee toda la lista de salvos y sus respectivos turnos
    con un solo id de GamePlayer*/
    private List<Map<String,Object>> getSalvoList(Game game){
        List<Map<String,Object>> myList = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> myList.addAll(makeSalvoList(gamePlayer.getSalvoes())));
        return myList;
    }


    //Mapping para poder agregar los ships con sus respectivas validaciones
    @RequestMapping(value="/games/players/{gamePlayer_id}/ships", method=RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> addShips(@PathVariable long gamePlayer_id, Authentication authentication, @RequestBody List<Ship> ships) {
        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        Optional<GamePlayer> gamePlayer = gamePlayerRepository.findById(gamePlayer_id);
        if (!gamePlayer.isPresent())
            return new ResponseEntity<>(makeMap("error", "This Game Doesn't Exist"), HttpStatus.UNAUTHORIZED);

        if(!gamePlayer.get().getPlayer().getUserName().equals(authentication.getName())){
            return new ResponseEntity<>(makeMap("error", "This Game Isn't Yours"), HttpStatus.UNAUTHORIZED);
        }
        if(gamePlayer.get().getPlayer().getId() != playerRepository.findByUserName(authentication.getName()).getId()){
            return new ResponseEntity<>(makeMap("error", "This Game Isn't Yours"), HttpStatus.UNAUTHORIZED);
        }
        if(gamePlayer.get().getShips().size() >= 5) {
            return new ResponseEntity<>(makeMap("error", "Your Ships Are Placed"), HttpStatus.FORBIDDEN);
        }

        for (Ship ship : ships) {
            ship.setGamePlayer(gamePlayer.get());
            shipRepository.save(ship);
        }
        return new ResponseEntity<>(makeMap("Ok!", "Ships Placed! :D"), HttpStatus.OK);
    }


    //Mapping para poder agregar los salvos con sus respectivas validaciones y turnos
    @RequestMapping(value="/games/players/{gamePlayer_id}/salvoes", method=RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> addSalvoes(@PathVariable long gamePlayer_id, Authentication authentication, @RequestBody Salvo salvo) {
        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        Optional<GamePlayer> gamePlayer = gamePlayerRepository.findById(gamePlayer_id);
        if (!gamePlayer.isPresent())
            return new ResponseEntity<>(makeMap("error", "This Game Doesn't Exist"), HttpStatus.UNAUTHORIZED);

        if (!gamePlayer.get().getPlayer().getUserName().equals(authentication.getName())) {
            return new ResponseEntity<>(makeMap("error", "This Game Isn't Yours"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.get().getPlayer().getId() != playerRepository.findByUserName(authentication.getName()).getId()) {
            return new ResponseEntity<>(makeMap("error", "This Game Isn't Yours"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.get().getSalvoes().stream().anyMatch(item -> item.getTurn() == salvo.getTurn())) {
            return new ResponseEntity<>(makeMap("error", "You Have Already Placed Your Shots"), HttpStatus.FORBIDDEN);
        } else {
            salvo.setGamePlayer(gamePlayer.get());
            salvoRepository.save(salvo);
            return new ResponseEntity<>(makeMap("Ok!", "Salvoes Placed! :D"), HttpStatus.CREATED);
        }
    }


    @RequestMapping("/leaderBoard")
    public List<Map<String,Object>> makeLeaderBoard(){
        return playerRepository
                .findAll()
                .stream()
                .map(player -> playerLeaderBoardDTO(player))
                .collect(Collectors.toList());

    }

    private Map<String, Object> playerLeaderBoardDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("userName", player.getUserName());
        dto.put("score", getScoreList(player));
        return dto;
    }

    private Map<String, Object> getScoreList (Player player) {
        Map<String,Object> dto = new LinkedHashMap<>();
        dto.put("total", player.getTotalScore());
        dto.put("won", player.getWins());
        dto.put("tied", player.getDraws());
        dto.put("lost", player.getLoses());
        return dto;
    }
}