package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SalvoApplication extends SpringBootServletInitializer {

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Autowired
	PasswordEncoder passwordEncoder;{}

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerRepository,
									  GameRepository gameRepository,
									  GamePlayerRepository gamePlayerRepository,
									  ShipRepository shipRepository,
									  SalvoRepository salvoRepository,
									  ScoreRepository scoreRepository) {
		 return (args) -> {

			 // save a couple of players
			 Player player1 = new Player("j.bauer@ctu.gov", passwordEncoder().encode("24"));
			 Player player2 = new Player("c.obrian@ctu.gov", passwordEncoder().encode("42"));
			 Player player3 = new Player("kim.bauer@gmail.com", passwordEncoder().encode("kb"));
			 Player player4 = new Player("k.almeida@ctu.gov", passwordEncoder().encode("mole"));

			 playerRepository.save(player1);
			 playerRepository.save(player2);
			 playerRepository.save(player3);
			 playerRepository.save(player4);


			 // save 3 games

			 Date fecha1 = new Date();
			 Game game1 = new Game();
			 game1.setCreationDate(fecha1);
			 Date fecha2 = fecha1.from(fecha1.toInstant().plusSeconds(3600));
			 Game game2 = new Game();
			 game2.setCreationDate(fecha2);
			 Date fecha3 = fecha2.from(fecha2.toInstant().plusSeconds(3600));
			 Game game3 = new Game();
			 game3.setCreationDate(fecha3);
			 Date fecha4 = fecha3.from(fecha3.toInstant().plusSeconds(3600));
			 Game game4 = new Game();
			 game4.setCreationDate(fecha4);
			 Date fecha5 = fecha4.from(fecha4.toInstant().plusSeconds(3600));
			 Game game5 = new Game();
			 game5.setCreationDate(fecha5);
			 Date fecha6 = fecha5.from(fecha5.toInstant().plusSeconds(3600));
			 Game game6 = new Game();
			 game6.setCreationDate(fecha6);
			 Date fecha7 = fecha6.from(fecha6.toInstant().plusSeconds(3600));
			 Game game7 = new Game();
			 game7.setCreationDate(fecha7);
			 Date fecha8 = fecha7.from(fecha7.toInstant().plusSeconds(3600));
			 Game game8 = new Game();
			 game8.setCreationDate(fecha8);

			 gameRepository.save(game1);
			 gameRepository.save(game2);
			 gameRepository.save(game3);
			 gameRepository.save(game4);
			 gameRepository.save(game5);
			 gameRepository.save(game6);
			 gameRepository.save(game7);
			 gameRepository.save(game8);


			 // save 3 games with 2 players

			 GamePlayer gamePlayer1 = new GamePlayer(game1,player1);
			 GamePlayer gamePlayer2 = new GamePlayer(game1,player2);
			 GamePlayer gamePlayer3 = new GamePlayer(game2,player1);
			 GamePlayer gamePlayer4 = new GamePlayer(game2,player2);
			 GamePlayer gamePlayer5 = new GamePlayer(game3,player2);
			 GamePlayer gamePlayer6 = new GamePlayer(game3,player4);
			 GamePlayer gamePlayer7 = new GamePlayer(game4,player2);
			 GamePlayer gamePlayer8 = new GamePlayer(game4,player1);
			 GamePlayer gamePlayer9 = new GamePlayer(game5,player4);
			 GamePlayer gamePlayer10 = new GamePlayer(game5,player1);
			 GamePlayer gamePlayer11 = new GamePlayer(game6,player3);
			 GamePlayer gamePlayer15 = new GamePlayer(game7,player3);
			 GamePlayer gamePlayer16 = new GamePlayer(game8,player4);


			 gamePlayerRepository.save(gamePlayer1);
			 gamePlayerRepository.save(gamePlayer2);
			 gamePlayerRepository.save(gamePlayer3);
			 gamePlayerRepository.save(gamePlayer4);
			 gamePlayerRepository.save(gamePlayer5);
			 gamePlayerRepository.save(gamePlayer6);
			 gamePlayerRepository.save(gamePlayer7);
			 gamePlayerRepository.save(gamePlayer8);
			 gamePlayerRepository.save(gamePlayer9);
			 gamePlayerRepository.save(gamePlayer10);
			 gamePlayerRepository.save(gamePlayer11);
			 gamePlayerRepository.save(gamePlayer15);
			 gamePlayerRepository.save(gamePlayer16);


			 //Save Ships with their locations


			 List<String> shipLocation1 = new ArrayList<>();
			 shipLocation1.add("H2");
			 shipLocation1.add("H3");
			 shipLocation1.add("H4");
			 List<String> shipLocation2 = new ArrayList<>();
			 shipLocation2.add("E1");
			 shipLocation2.add("F1");
			 shipLocation2.add("G1");
			 List<String> shipLocation3 = new ArrayList<>();
			 shipLocation3.add("B4");
			 shipLocation3.add("B5");
			 List<String> shipLocation4 = new ArrayList<>();
			 shipLocation4.add("B5");
			 shipLocation4.add("C5");
			 shipLocation4.add("D5");
			 List<String> shipLocation5 = new ArrayList<>();
			 shipLocation5.add("F1");
			 shipLocation5.add("F2");
			 List<String> shipLocation6 = new ArrayList<>();
			 shipLocation6.add("B5");
			 shipLocation6.add("C5");
			 shipLocation6.add("D5");
			 List<String> shipLocation7 = new ArrayList<>();
			 shipLocation7.add("C6");
			 shipLocation7.add("C7");
			 List<String> shipLocation8 = new ArrayList<>();
			 shipLocation8.add("A2");
			 shipLocation8.add("A3");
			 shipLocation8.add("A4");
			 List<String> shipLocation9 = new ArrayList<>();
			 shipLocation9.add("G6");
			 shipLocation9.add("H6");
			 List<String> shipLocation10 = new ArrayList<>();
			 shipLocation10.add("B5");
			 shipLocation10.add("C5");
			 shipLocation10.add("D5");
			 List<String> shipLocation11 = new ArrayList<>();
			 shipLocation11.add("C6");
			 shipLocation11.add("C7");
			 List<String> shipLocation12 = new ArrayList<>();
			 shipLocation12.add("A2");
			 shipLocation12.add("A3");
			 shipLocation12.add("A4");
			 List<String> shipLocation13 = new ArrayList<>();
			 shipLocation13.add("G6");
			 shipLocation13.add("H6");
			 List<String> shipLocation14 = new ArrayList<>();
			 shipLocation14.add("B5");
			 shipLocation14.add("C5");
			 shipLocation14.add("D5");
			 List<String> shipLocation15 = new ArrayList<>();
			 shipLocation15.add("C6");
			 shipLocation15.add("C7");
			 List<String> shipLocation16 = new ArrayList<>();
			 shipLocation16.add("A2");
			 shipLocation16.add("A3");
			 shipLocation16.add("A4");
			 List<String> shipLocation17 = new ArrayList<>();
			 shipLocation17.add("G6");
			 shipLocation17.add("H6");
			 List<String> shipLocation18 = new ArrayList<>();
			 shipLocation18.add("B5");
			 shipLocation18.add("C5");
			 shipLocation18.add("D5");
			 List<String> shipLocation19 = new ArrayList<>();
			 shipLocation19.add("C6");
			 shipLocation19.add("C7");
			 List<String> shipLocation20 = new ArrayList<>();
			 shipLocation20.add("A2");
			 shipLocation20.add("A3");
			 shipLocation20.add("A4");
			 List<String> shipLocation21 = new ArrayList<>();
			 shipLocation21.add("G6");
			 shipLocation21.add("H6");
			 List<String> shipLocation22 = new ArrayList<>();
			 shipLocation22.add("B5");
			 shipLocation22.add("C5");
			 shipLocation22.add("D5");
			 List<String> shipLocation23 = new ArrayList<>();
			 shipLocation23.add("C6");
			 shipLocation23.add("C7");
			 List<String> shipLocation24 = new ArrayList<>();
			 shipLocation24.add("B5");
			 shipLocation24.add("C5");
			 shipLocation24.add("D5");
			 List<String> shipLocation25 = new ArrayList<>();
			 shipLocation25.add("C6");
			 shipLocation25.add("C7");
			 List<String> shipLocation26 = new ArrayList<>();
			 shipLocation26.add("A2");
			 shipLocation26.add("A3");
			 shipLocation26.add("A4");
			 List<String> shipLocation27 = new ArrayList<>();
			 shipLocation27.add("G6");
			 shipLocation27.add("H6");

			 String shipType1 = "carrier";
			 String shipType2 = "battleship";
			 String shipType3 = "submarine";
			 String shipType4 = "destroyer";
			 String shipType5 = "patrol boat";

			 Ship ship1 = new Ship(shipType1, shipLocation1, gamePlayer1);
			 Ship ship2 = new Ship(shipType3, shipLocation2, gamePlayer1);
			 Ship ship3 = new Ship(shipType5, shipLocation3, gamePlayer1);
			 Ship ship4 = new Ship(shipType4, shipLocation4, gamePlayer2);
			 Ship ship5 = new Ship(shipType5, shipLocation5, gamePlayer2);
			 Ship ship6 = new Ship(shipType4, shipLocation6, gamePlayer3);
			 Ship ship7 = new Ship(shipType5, shipLocation7, gamePlayer3);
			 Ship ship8 = new Ship(shipType3, shipLocation8, gamePlayer4);
			 Ship ship9 = new Ship(shipType5, shipLocation9, gamePlayer4);
			 Ship ship10 = new Ship(shipType4, shipLocation10, gamePlayer5);
			 Ship ship11 = new Ship(shipType5, shipLocation11, gamePlayer5);
			 Ship ship12 = new Ship(shipType3, shipLocation12, gamePlayer6);
			 Ship ship13 = new Ship(shipType5, shipLocation13, gamePlayer6);
			 Ship ship14 = new Ship(shipType1, shipLocation14, gamePlayer7);
			 Ship ship15 = new Ship(shipType5, shipLocation15, gamePlayer7);
			 Ship ship16 = new Ship(shipType3, shipLocation16, gamePlayer8);
			 Ship ship17 = new Ship(shipType5, shipLocation17, gamePlayer8);
			 Ship ship18 = new Ship(shipType4, shipLocation18, gamePlayer9);
			 Ship ship19 = new Ship(shipType5, shipLocation19, gamePlayer9);
			 Ship ship20 = new Ship(shipType3, shipLocation20, gamePlayer10);
			 Ship ship21 = new Ship(shipType5, shipLocation21, gamePlayer10);
			 Ship ship22 = new Ship(shipType4, shipLocation22, gamePlayer11);
			 Ship ship23 = new Ship(shipType2, shipLocation23, gamePlayer11);
			 Ship ship24 = new Ship(shipType3, shipLocation24, gamePlayer15);
			 Ship ship25 = new Ship(shipType5, shipLocation25, gamePlayer15);
			 Ship ship26 = new Ship(shipType3, shipLocation26, gamePlayer16);
			 Ship ship27 = new Ship(shipType2, shipLocation27, gamePlayer16);


			 shipRepository.save(ship1);
			 shipRepository.save(ship2);
			 shipRepository.save(ship3);
			 shipRepository.save(ship4);
			 shipRepository.save(ship5);
			 shipRepository.save(ship6);
			 shipRepository.save(ship7);
			 shipRepository.save(ship8);
			 shipRepository.save(ship9);
			 shipRepository.save(ship10);
			 shipRepository.save(ship11);
			 shipRepository.save(ship12);
			 shipRepository.save(ship13);
			 shipRepository.save(ship14);
			 shipRepository.save(ship15);
			 shipRepository.save(ship16);
			 shipRepository.save(ship17);
			 shipRepository.save(ship18);
			 shipRepository.save(ship19);
			 shipRepository.save(ship20);
			 shipRepository.save(ship21);
			 shipRepository.save(ship22);
			 shipRepository.save(ship23);
			 shipRepository.save(ship24);
			 shipRepository.save(ship25);
			 shipRepository.save(ship26);
			 shipRepository.save(ship27);

			 //Save Salvoes


			 int turn1 = 1;
			 int turn2 = 2;
			 int turn3 = 3;

			 List<String> locationsSalvo1 = new ArrayList<>();
			 locationsSalvo1.add("B5");
			 locationsSalvo1.add("C5");
			 locationsSalvo1.add("F1");
			 List<String> locationsSalvo2 = new ArrayList<>();
			 locationsSalvo2.add("B4");
			 locationsSalvo2.add("B5");
			 locationsSalvo2.add("B6");
			 List<String> locationsSalvo3 = new ArrayList<>();
			 locationsSalvo3.add("F2");
			 locationsSalvo3.add("D5");
			 List<String> locationsSalvo4 = new ArrayList<>();
			 locationsSalvo4.add("E1");
			 locationsSalvo4.add("H3");
			 locationsSalvo4.add("A2");
			 List<String> locationsSalvo5 = new ArrayList<>();
			 locationsSalvo5.add("A2");
			 locationsSalvo5.add("A4");
			 locationsSalvo5.add("G6");
			 List<String> locationsSalvo6 = new ArrayList<>();
			 locationsSalvo6.add("B5");
			 locationsSalvo6.add("D5");
			 locationsSalvo6.add("C7");
			 List<String> locationsSalvo7 = new ArrayList<>();
			 locationsSalvo7.add("A3");
			 locationsSalvo7.add("H6");
			 List<String> locationsSalvo8 = new ArrayList<>();
			 locationsSalvo8.add("C5");
			 locationsSalvo8.add("C6");
			 List<String> locationsSalvo9 = new ArrayList<>();
			 locationsSalvo9.add("G6");
			 locationsSalvo9.add("H6");
			 locationsSalvo9.add("A4");
			 List<String> locationsSalvo10 = new ArrayList<>();
			 locationsSalvo10.add("H1");
			 locationsSalvo10.add("H2");
			 locationsSalvo10.add("H3");
			 List<String> locationsSalvo11 = new ArrayList<>();
			 locationsSalvo11.add("A2");
			 locationsSalvo11.add("A3");
			 locationsSalvo11.add("D8");
			 List<String> locationsSalvo12 = new ArrayList<>();
			 locationsSalvo12.add("E1");
			 locationsSalvo12.add("F2");
			 locationsSalvo12.add("G3");
			 List<String> locationsSalvo13 = new ArrayList<>();
			 locationsSalvo13.add("A3");
			 locationsSalvo13.add("A4");
			 locationsSalvo13.add("F7");
			 List<String> locationsSalvo14 = new ArrayList<>();
			 locationsSalvo14.add("B5");
			 locationsSalvo14.add("C6");
			 locationsSalvo14.add("H1");
			 List<String> locationsSalvo15 = new ArrayList<>();
			 locationsSalvo15.add("A2");
			 locationsSalvo15.add("G6");
			 locationsSalvo15.add("H6");
			 List<String> locationsSalvo16 = new ArrayList<>();
			 locationsSalvo16.add("C5");
			 locationsSalvo16.add("C7");
			 locationsSalvo16.add("D5");
			 List<String> locationsSalvo17 = new ArrayList<>();
			 locationsSalvo17.add("A1");
			 locationsSalvo17.add("A2");
			 locationsSalvo17.add("A3");
			 List<String> locationsSalvo18 = new ArrayList<>();
			 locationsSalvo18.add("B5");
			 locationsSalvo18.add("B6");
			 locationsSalvo18.add("C7");
			 List<String> locationsSalvo19 = new ArrayList<>();
			 locationsSalvo19.add("G6");
			 locationsSalvo19.add("G7");
			 locationsSalvo19.add("G8");
			 List<String> locationsSalvo20 = new ArrayList<>();
			 locationsSalvo20.add("C6");
			 locationsSalvo20.add("D6");
			 locationsSalvo20.add("E6");
			 List<String> locationsSalvo21 = new ArrayList<>();
			 locationsSalvo21.add("H1");
			 locationsSalvo21.add("H8");

			 Salvo salvo1 = new Salvo(gamePlayer1,turn1, locationsSalvo1);
			 Salvo salvo2 = new Salvo(gamePlayer2,turn1, locationsSalvo2);
			 Salvo salvo3 = new Salvo(gamePlayer1,turn2, locationsSalvo3);
			 Salvo salvo4 = new Salvo(gamePlayer2,turn2, locationsSalvo4);
			 Salvo salvo5 = new Salvo(gamePlayer3,turn1, locationsSalvo5);
			 Salvo salvo6 = new Salvo(gamePlayer4,turn1, locationsSalvo6);
			 Salvo salvo7 = new Salvo(gamePlayer3,turn2, locationsSalvo7);
			 Salvo salvo8 = new Salvo(gamePlayer4,turn2, locationsSalvo8);
			 Salvo salvo9 = new Salvo(gamePlayer5,turn1, locationsSalvo9);
			 Salvo salvo10 = new Salvo(gamePlayer6,turn1, locationsSalvo10);
			 Salvo salvo11 = new Salvo(gamePlayer5,turn2, locationsSalvo11);
			 Salvo salvo12 = new Salvo(gamePlayer6,turn2, locationsSalvo12);
			 Salvo salvo13 = new Salvo(gamePlayer7,turn1, locationsSalvo13);
			 Salvo salvo14 = new Salvo(gamePlayer8,turn1, locationsSalvo14);
			 Salvo salvo15 = new Salvo(gamePlayer7,turn2, locationsSalvo15);
			 Salvo salvo16 = new Salvo(gamePlayer8,turn2, locationsSalvo16);
			 Salvo salvo17 = new Salvo(gamePlayer9,turn1, locationsSalvo17);
			 Salvo salvo18 = new Salvo(gamePlayer10,turn1, locationsSalvo18);
			 Salvo salvo19 = new Salvo(gamePlayer9,turn2, locationsSalvo19);
			 Salvo salvo20 = new Salvo(gamePlayer10,turn2, locationsSalvo20);
			 Salvo salvo21 = new Salvo(gamePlayer10,turn3, locationsSalvo21);


			 salvoRepository.save(salvo1);
			 salvoRepository.save(salvo2);
			 salvoRepository.save(salvo3);
			 salvoRepository.save(salvo4);
			 salvoRepository.save(salvo5);
			 salvoRepository.save(salvo6);
			 salvoRepository.save(salvo7);
			 salvoRepository.save(salvo8);
			 salvoRepository.save(salvo9);
			 salvoRepository.save(salvo10);
			 salvoRepository.save(salvo11);
			 salvoRepository.save(salvo12);
			 salvoRepository.save(salvo13);
			 salvoRepository.save(salvo14);
			 salvoRepository.save(salvo15);
			 salvoRepository.save(salvo16);
			 salvoRepository.save(salvo17);
			 salvoRepository.save(salvo18);
			 salvoRepository.save(salvo19);
			 salvoRepository.save(salvo20);
			 salvoRepository.save(salvo21);


			 //Save Scores

			 float win = 1;
			 float lose = 0;
			 float tie = (float)0.5;

			 Date finishDate1 = fecha1.from(fecha1.toInstant().plusSeconds(1800));
			 Date finishDate2 = fecha2.from(fecha2.toInstant().plusSeconds(1800));
			 Date finishDate3 = fecha3.from(fecha3.toInstant().plusSeconds(1800));
			 Date finishDate4 = fecha4.from(fecha4.toInstant().plusSeconds(1800));
			 Date finishDate5 = fecha5.from(fecha5.toInstant().plusSeconds(1800));
			 Date finishDate6 = fecha6.from(fecha6.toInstant().plusSeconds(1800));
			 Date finishDate7 = fecha7.from(fecha7.toInstant().plusSeconds(1800));
			 Date finishDate8 = fecha8.from(fecha8.toInstant().plusSeconds(1800));

			 Score score1 = new Score (finishDate1, player1, game1, win);
			 Score score2 = new Score (finishDate1, player2, game1, lose);
			 Score score3 = new Score (finishDate2, player1, game2, tie);
			 Score score4 = new Score (finishDate2, player2, game2, tie);
			 Score score5 = new Score (finishDate3, player2, game3, win);
			 Score score6 = new Score (finishDate3, player4, game3, lose);
			 Score score7 = new Score (finishDate4, player2, game4, tie);
			 Score score8 = new Score (finishDate4, player1, game4, tie);


			 scoreRepository.save(score1);
			 scoreRepository.save(score2);
			 scoreRepository.save(score3);
			 scoreRepository.save(score4);
			 scoreRepository.save(score5);
			 scoreRepository.save(score6);
			 scoreRepository.save(score7);
			 scoreRepository.save(score8);

		 };
		 }
	}

@EnableWebSecurity
@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	PlayerRepository playerRepository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(username -> {
			Player player = playerRepository.findByUserName(username);
			if (player != null) {
				return new User(player.getUserName(), player.getPassword(),
						AuthorityUtils.createAuthorityList("USER"));
			} else {
				throw new UsernameNotFoundException("Unknown user: " + username);
			}
		});
	}
}

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/web/games_3.html").permitAll()
				.antMatchers("/web/**").permitAll()
				.antMatchers("/api/games").permitAll()
				.antMatchers("/api/players").permitAll()
				.antMatchers("/api/game_view/*").hasAuthority("USER")
				.antMatchers("/rest/*").denyAll()
				.anyRequest().permitAll();

		http.formLogin()
				.usernameParameter("username")
				.passwordParameter("password")
				.loginPage("/api/login");

		http.logout().logoutUrl("/api/logout");

		// turn off checking for CSRF tokens
		http.csrf().disable();

		// if user is not authenticated, just send an authentication failure response
		http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if login is successful, just clear the flags asking for authentication
		http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

		// if login fails, just send an authentication failure response
		http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if logout is successful, just send a success response
		http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
	}

	private void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		}
	}
}
