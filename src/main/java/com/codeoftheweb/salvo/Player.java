package com.codeoftheweb.salvo;

import javax.persistence.*;
import java.util.List;

@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
    private long id;

    private String userName;
    private String password;

    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    private
    List<GamePlayer> gamePlayers;

    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    private List<Score> scores;

    public Player () { }

    public Player(String userName, String password)
    { this.userName = userName;
      this.password = password;
    }

    public void addScore(Score score) {
        score.setPlayer(this);
        getScores().add(score);
    }

    //Para obtener un solo score para el juego respectivo (necesario para el js)
    public Score getScore(Game game) {
        return game.getScores().stream().filter(score -> score.getPlayer().equals(this)).findFirst().orElse(null);
    }

    //Para obtener la lista de Scores (Wins, Loses, Draws) junto con su total
    public float getTotalScore() {
        return this.getWins()*1 + this.getDraws()*((float) 0.5) + this.getLoses()*0;}


    public long getWins(){
        return scores
                .stream()
                .filter(score -> score.getScore() == 1)
                .count();
    }

    public long getDraws(){
        return scores
                .stream()
                .filter(score -> score.getScore() == 0.5)
                .count();
    }

    public long getLoses(){
        return scores
                .stream()
                .filter(score -> score.getScore() == 0)
                .count();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id; }

    public void setId(long id) {
        this.id = id;
    }

    public List<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }

    public void setGamePlayers(List<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

}
