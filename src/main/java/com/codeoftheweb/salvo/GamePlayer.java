package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
public class GamePlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private Date joinDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;

    @OneToMany(mappedBy = "gamePlayer")
    private List<Ship> ships;

    @OneToMany(mappedBy = "gamePlayer")
    private List<Salvo> salvoes;

    public GamePlayer() {
    }

    public GamePlayer(Game game, Player player) {
        this.game = game;
        this.player = player;
        this.setJoinDate(new Date());
    }

    //Método para poder obtener los Score en el controller desde GamePlayer
    public Score getScore() {
        return getPlayer().getScore(getGame());
    }

    //Método para poder obtener los GameState (para lo que primero necesitamos el opponent player y los sinks)
    enum GameState {
        WAIT_OPPONENT_JOIN,
        PLACE_SHIPS,
        WAIT_OPPONENT_SHIPS,
        ENTER_SALVOES,
        WAIT_OPPONENT_SALVOES,
        WIN,
        LOSE,
        DRAW,
        UNDEFINED
    }

    public Enum<GameState> getGameState() {
        Enum<GameState> gameStateEnum = GameState.UNDEFINED;
        Optional<GamePlayer> opponentGP = game.getGamePlayers().stream().filter(gpo -> gpo.getId() != this.getId()).findFirst();
        GamePlayer opponentGamePlayer = opponentGP.get();
        if (!opponentGP.isPresent()) {
            gameStateEnum = GameState.WAIT_OPPONENT_JOIN;
        } else {
            if (this.getShips().isEmpty())
                gameStateEnum = GameState.PLACE_SHIPS;
            else if (opponentGamePlayer.getShips().isEmpty())
                gameStateEnum = GameState.WAIT_OPPONENT_SHIPS;
            else {
                int selfTurn = this.getSalvoes().stream().mapToInt(Salvo::getTurn).max().orElse(0);
                int opponentTurn = opponentGamePlayer.getSalvoes().stream().mapToInt(Salvo::getTurn).max().orElse(0);
                if (selfTurn < opponentTurn){
                    gameStateEnum = GameState.ENTER_SALVOES;
                }
                else if (selfTurn > opponentTurn) {
                    gameStateEnum = GameState.WAIT_OPPONENT_SALVOES;
                }
                else if (selfTurn == opponentTurn) {
                    gameStateEnum = GameState.WAIT_OPPONENT_SALVOES;
                }
            }
        }
        return gameStateEnum;
    }

    private List<Map<String, Object>> getSinks (int turn, List<Ship> transformers, Set<Salvo> salvoes){
        List<String> allShots = new ArrayList<>();
        salvoes.stream()
                .filter(salvo -> salvo.getTurn() <= turn)
                .forEach(salvo -> allShots.addAll(salvo.getShots()));

        return transformers.stream()
                .filter(trf -> allShots.containsAll(trf.getCells()))
                .map(Transformer::transformersDTO)
                .collect(Collectors.toList());
    }



    public Date getJoinDate()
    { return joinDate;
    }

    public void setJoinDate(Date joinDate)
    {this.joinDate = joinDate; }

    public Player getPlayer()
    { return player;
    }

    public Game getGame()
    { return game;
    }

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }


    public List<Salvo> getSalvoes() {
        return salvoes;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
